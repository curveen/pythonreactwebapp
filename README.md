# Frontend - REACT WEB

cd client
npm install
npm start

# Backend - PYTHON SERVER

## first time only
python -m venv venv

source venv/Scripts/activate
cd flask-server
pip install -r requirements.txt
python server.py