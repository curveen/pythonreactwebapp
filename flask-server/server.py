from flask import Flask
import SchemeDrawDemo
from SchemeDrawDemo import FlowDiagram

app = Flask(__name__)

# Members API route
@app.route("/members")
def members():
    return {"members":["Member1","Member2"]}

@app.route("/schemedraw")
def schemedraw():
    return FlowDiagram.drawModel()

if __name__ == "__main__":
    app.run(debug=True)