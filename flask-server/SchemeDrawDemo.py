
from flask import Response
import schemdraw
from schemdraw import flow

class FlowDiagram(object):

    def __init__(self):
        self.model = None
        
    def drawModel():   
        with schemdraw.Drawing(show=False) as d:
            d+= (flow.Start(w=5).label("Go To New Billing")).label("test", loc='top').fill("yellow")
            d+= flow.Arrow().down(d.unit/2).label("initial", loc='right').color('mediumblue')
            #Input the string 
            d+= (step1:= flow.Data(w = 4).label("Enter a string:\n string").label("sequent", loc='right').color('deeppink'))
            
            d+= flow.Arrow().right(d.unit/2).at(step1.E)
            d+= flow.Subroutine(w = 4).label("submodel")
            
            d+= flow.Arrow().down(d.unit/2).at(step1.S)
            
            d+= flow.Subroutine(w = 4).label("submodel")
            
            d+= flow.Arrow().down(d.unit/2).at(step1.S)
            
            d+= flow.Subroutine(w = 4).label("submodel 2")

            d+= flow.Arrow().down(d.unit/2)

            #Reverse the string
            d+= flow.Box(w = 4).label("Reverse the string:\n reverse_string")
            d+= flow.Arrow().down(d.unit/2)
            
            #Check if string and reverse_string are same
            d+= (decision := flow.Decision(w = 5, h= 5,
                            S = "True",
                                E = "False").label("Is \n string\n == \nreverse_string?"))
            
            #If True
            d+= flow.Arrow().length(d.unit/2)
            d+= (true := flow.Box(w = 5).label("string is a palindrome."))
            d+= flow.Arrow().length(d.unit/2)
            
            #End program
            d+= (end := flow.Ellipse().label("End"))
            
            #If False. Start the arrow from East of decision box
            d+= flow.Arrow().right(d.unit).at(decision.E)
            
            #false is referring to the box where string is not a palindrome.
            d+= (false := flow.Box(w = 5).label("string is not\n a palindrome."))
            
            #Add a downward arrow from the South of false box 
            d+= flow.Arrow().down(d.unit*2.5).at(false.S)
            
            #Extend the arrow to reach the end of the program
            d+= flow.Arrow().left(d.unit*2.15)
          
            return Response(
            response=d.get_imagedata("svg"),
            status=200,
            mimetype="image/svg+xml",
            headers={
                "Content-disposition": "attachment; filename=diagram.svg"
            },
            )