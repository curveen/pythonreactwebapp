import React, { useState, useEffect } from 'react'
// added proxy in package.json to link backend
// useState = save state of data
// useEffect = fetch backend data on first render
function App() {

  const [diagram, setDiagram] = useState(null)
  
  useEffect(()=>{
    fetch("/schemedraw").then(
      res => res.json()
    ).then(
      data => {
        setDiagram(data)
        console.log(data)
       }
    )
  },[])
  return (
    <div>
      {/* {(typeof data.members === 'undefined')?(
        <p> Loading ...</p>
      ):(
        data.members.map((member, i)=> (
          <p key={i}>{member}</p>
        ))
      )
      } */}
      {(typeof diagram === 'undefined')?(
        <p> Loading ...</p>
      ):(
        <img src="/schemedraw" alt="flow diagram"/>
      )
      }
    </div>
  )
  
}

export default App